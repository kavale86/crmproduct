<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('common.head')
</head>

<body class="nav-md"> <!-- body -->
<div class="container body"> <!-- container body -->
    <div class="main_container"> <!-- main container -->

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('/') }}" class="site_title">
                        <span>
                            {{ config('app.name', 'Laravel') }}
                        </span>
                    </a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_info">
                        <h2><h2>{{ Auth::user()->name }}</h2></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                @include('common.menu')
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                    <ul class=" navbar-right">
                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{ isset(auth::user()->avatar)? asset(auth::user()->avatar) : asset('uploads/users/default_avatar.png') }}" alt="">{{ auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                @can('superAdmin')
                                    <a href="{{route('users.edit', auth::user())}}" class="dropdown-item">
                                        Edit profile
                                        <i class="fa fa-user pull-right"></i>
                                    </a>
                                 @endcan
                                <a class="dropdown-item"
                                   href="l{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                >

                                    <i class="fa fa-sign-out pull-right"></i>
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <footer>
            <div class="pull-right">
                <div id="wds-credential-links" class="wds-credential-links">
                    <a id="wds-credential-link-webdesignsun" class="wds-credential-link-webdesignsun" href="https://www.webdesignsun.com" title="Designed with Soul and Care by Web Design Sun®" target="_blank">Designed by Web Design Sun®</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </footer>
    </div><!-- main container -->
</div> <!-- container body -->

<!-- scripts -->


<script src="{{asset('js/gentelella.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>

</body>
</html>
