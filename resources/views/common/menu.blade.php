<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section active">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li>
                <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home </a>
            </li>
            @can('superAdmin')
            <li>
                <a href="{{ route('admins.index') }}"><i class="fa fa-users"></i> List of admins </a>
            </li>
            @endcan
            @can('admin')
            <li>
                <a href="{{ route('products.create') }}"><i class="fa fa-cube"></i> Add New Product </a>
            </li>
            <li>
                <a href="{{ route('products.index') }}"><i class="fa fa-cubes"></i> List of Products </a>
            </li>

            <li>
                <a href="{{ route('developers.index') }}"><i class="fa fa-code"></i> List of Developers </a>
            </li>
            <li>
                <a href="{{ route('clients.index') }}"><i class="fa fa-user"></i> List of Clients </a>
            </li>
            <li>
                <a href="{{ route('kpi') }}"><i class="fa fa-pie-chart"></i> Head of KPI </a>
            </li>
            @endcan
            @if(auth()->user()->role == \App\Models\User::ROLE_DEVELOPER)
                <li>
                    <a href="{{ route('developers.show', auth()->user()->id) }}"><i class="fa fa-code"></i> Developer page </a>
                </li>
            @endif
        </ul>
    </div>
</div>
