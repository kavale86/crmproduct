<div class="file_block">
    <input type="hidden"  name="files[]" value="{{ $path }}" class="input_file">
    <div class="file_name_block">
        <span class="file_name">{{ $name }}</span>
        <span class="delete_file"><i class="fa fa-remove"></i></span>
    </div>
</div>
