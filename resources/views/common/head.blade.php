<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title')</title>
<meta name="designer" content="Designed with Soul and Care by Web Design Sun® | https://www.webdesignsun.com">
<script src="https://kit.fontawesome.com/1498b7a97f.js" crossorigin="anonymous"></script>
<!-- Styles -->
<link href="{{ asset('css/gentelella.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">

<script>
    let show_modal;
</script>

@yield('here_maps')
