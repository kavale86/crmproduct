<div class="row top_home_block">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-cubes"></i>
            </div>
            <div class="count">{{ $count_products }}</div>
            <h3>Total Products</h3>
            <p>That the client develops</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fas fa-clock-o"></i>
            </div>
            <div class="count">{{ $average_time }} days</div>
            <h3>Average Time</h3>
            <p>From Project Confirmed to Goods payed</p>
        </div>
    </div>
</div>
