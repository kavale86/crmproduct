<div id="user_add_modal"  class="success_modal popup mfp-hide">
    @if(session('success'))
    <div class="x_title">
        <h4 class="text-center">{{  session('success')['text'] }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('clients.index') }}" class="btn btn-secondary btn-sm width_100">Back to Clients List</a>
        </div>
        <div class="col-md-12">
            <a href="{{ route('clients.create') }}" class="btn btn-secondary btn-sm width_100">Add Another One</a>
        </div>
    </div>
    @endif
</div>
