<div class="row top_home_block">
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 ">
        <a class="tile-stats {{ $kpi == \App\Models\Product::WEEK_COUNT? 'active': '' }} kpi_block" href="{{ route('kpi', ['kpi'=> \App\Models\Product::WEEK_COUNT ]) }}">
            <div class="icon"><i class="fa fa-cubes"></i>
            </div>
            <div class="count">{{ $count_products }}</div>
            <h3>Total Products</h3>
            <p>That the client develops</p>
        </a>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
        <a class="tile-stats {{ $kpi == \App\Models\Product::WEEK_AVERAGE? 'active': '' }}  kpi_block" href="{{ route('kpi', ['kpi'=> \App\Models\Product::WEEK_AVERAGE ]) }}">
            <div class="icon"><i class="fas fa-clock-o"></i>
            </div>
            <div class="count">{{ $arrayTime['average_time'] }} days</div>
            <h3>Average Time</h3>
            <p>From Project Confirmed to Goods payed</p>
        </a>
    </div>
</div>
<div class="row top_home_block">
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
        <a class="tile-stats {{ $kpi == \App\Models\Product::WEEK_TIME? 'active': '' }}  kpi_block" href="{{ route('kpi', ['kpi'=> \App\Models\Product::WEEK_TIME ]) }}">
            <div class="icon"><i class="fa fa-hourglass-half"></i>
            </div>
            <div class="count">{{ $arrayTime['in_time'] }} </div>
            <h3>Products % in Time</h3>
            <p>From Project Confirmed to Goods payed</p>
        </a>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
        <a class="tile-stats {{ $kpi == \App\Models\Product::WEEK_CRITICAL? 'active': '' }}  kpi_block" href="{{ route('kpi', ['kpi'=> \App\Models\Product::WEEK_CRITICAL ]) }}">
            <div class="icon"><i class="fa fa-hourglass-end"></i>
            </div>
            <div class="count">{{ $arrayTime['critical'] }}</div>
            <h3>Products % Critical</h3>
            <p>From Project Confirmed to Goods payed</p>
        </a>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
        <a class="tile-stats {{ $kpi == \App\Models\Product::WEEK_OVERTIME? 'active': '' }}  kpi_block" href="{{ route('kpi', ['kpi'=> \App\Models\Product::WEEK_OVERTIME ]) }}">
            <div class="icon"><i class="fa fa-hourglass-o"></i>
            </div>
            <div class="count">{{ $arrayTime['overtime'] }}</div>
            <h3>Products % Overtime</h3>
            <p>From Project Confirmed to Goods payed</p>
        </a>
    </div>
</div>
