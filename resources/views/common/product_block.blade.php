@if($products->isNotEmpty())
        @foreach( $products as $product)
            <div class="row product_block x_title">
                <div class="col-md-3">
                    <h4>{{ $product->client->name }} - {{ $product->name }}</h4>
                    <ul class="detail_order">
                        <li>
                            <span class="label">Product Number: </span>
                            <span class="value"><strong>{{ $product->id }}</strong></span>
                        </li>
                        <li>
                            <span class="label">Status: </span>
                            <span class="value"><strong>{{ empty($product->status)? 'no-status': $product->productStatus->description }}</strong></span>
                        </li>
                        <li>
                            <span class="label">Start: </span>
                            <span class="value"><strong>{{ empty($product->start_date)? '':$product->start_date->format('d.m.Y') }}</strong></span>
                        </li>
                        <li>
                            <span class="label">Product: </span>
                            <span class="value"><strong>{{ $product->name }}</strong></span>
                        </li>
                        <li>
                            <span class="label">Developer: </span>
                            <span class="value"><strong>{{ $product->developer->name }}</strong></span>
                        </li>
                        <li>
                            <span class="label">Client: </span>
                            <span class="value"><strong>{{ $product->client->name }}</strong></span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps anchor">
                            @php $width = round(100/count($statuses)) @endphp
                            @php $index = optional($product->productStatus)->index?: 0  @endphp
                            @foreach( $statuses as $status)
                                <li style="width: {{$width}}%;">
                                    <a  class="{{ $status->index <= $index? 'selected': 'disabled' }}">
                                        <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                        <span class="step_descr">
                                                {{ $status->description }}<br>
                                                <small>{{ !empty($product->status_report[$status->name])? $product->getDateStatus($product->status_report[$status->name]) :'' }}</small>
                                                </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
@else
    <div>Nothing found</div>
@endif
