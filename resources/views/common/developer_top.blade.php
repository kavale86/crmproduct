<div class="row top_home_block">
    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 ">
        <div class="tile-stats">
            <div class="user_block develop_user_block">
                <div class="user_action_block td_actions">
                    <a href="{{ route('developers.edit',$developer->id)}}" class="btn btn-primary btn-sm edit_post" ><i class="fa fa-pencil"></i></a>
                    <form action="{{ route('developers.destroy', $developer->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        <button class="btn btn-danger btn-sm delete_post"><i class="fa fa-trash-o"></i> </button>
                    </form>
                </div>
                <div class="img_block">
                    <img src="{{ $developer->avatar? asset($developer->avatar): asset('uploads/users/default_avatar.png') }}" alt="">
                </div>
                <div class="user_info">
                    <div class="title">Developer - {{ $developer->name }}</div>
                    <div class="email"><i class="fa fa-envelope"></i> {{ $developer->email }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-cubes"></i>
            </div>
            <div class="count">{{ $count_products }}</div>
            <h3>Total Products</h3>
            <p>That the client develops</p>
        </div>
    </div>
</div>
<div class="row top_home_block">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-hourglass-half"></i>
            </div>
            <div class="count">{{ $arrayTime['in_time'] }} </div>
            <h3>Products % in Time</h3>
            <p>From Project Confirmed to Goods payed</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-hourglass-end"></i>
            </div>
            <div class="count">{{ $arrayTime['critical'] }}</div>
            <h3>Products % Critical</h3>
            <p>From Project Confirmed to Goods payed</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-hourglass-o"></i>
            </div>
            <div class="count">{{ $arrayTime['overtime'] }}</div>
            <h3>Products % Overtime</h3>
            <p>From Project Confirmed to Goods payed</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fas fa-clock-o"></i>
            </div>
            <div class="count">{{ $arrayTime['average_time'] }} days</div>
            <h3>Average Time</h3>
            <p>From Project Confirmed to Goods payed</p>
        </div>
    </div>
</div>
