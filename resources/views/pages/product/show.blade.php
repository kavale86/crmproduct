@extends('layouts.layouts')

@section('content')

    @if (session('success'))
        <script>
            show_modal = true;
        </script>
    @else
        <script>
            show_modal = false;
        </script>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <h4>Product - {{ $product->name }} - №{{ $product->id }}</h4>
                </div>
                <div class="col-md-2">
                    @can('admin')
                    <div class="td_actions">
                        <a href="{{ route('products.edit',$product->id)}}" title="edit" class="btn btn-primary btn-xs edit_post" ><i class="fa fa-pencil"></i></a>
                        <form action="{{ route('products.destroy', $product->id)}}" method="post">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button class="btn btn-danger btn-xs delete_post"><i class="fa fa-trash-o"></i> </button>
                        </form>
                    </div>
                     @endcan
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content product_show">
                    <div class="row">
                        <div class="col-md-6 col_border_right">
                            <h6>Product Name: {{ $product->name }}</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="user_block">
                                        <div class="img_block">
                                            <img src="{{ $product->developer->avatar? asset($product->developer->avatar): asset('uploads/users/default_avatar.png') }}" alt="">
                                        </div>
                                        <div class="user_info">
                                            <div class="title">Developer - {{ $product->developer->name }}</div>
                                            <div class="email"><i class="fa fa-envelope"></i> {{ $product->developer->email }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="user_block">
                                        <div class="img_block">
                                            <img src="{{ $product->client->avatar? asset($product->client->avatar): asset('uploads/users/default_avatar.png') }}" alt="">
                                        </div>
                                        <div class="user_info">
                                            <div class="title">Client - {{ $product->client->name }}</div>
                                            <div class="email"><i class="fa fa-envelope"></i> {{ $product->client->email }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <h4>Description:</h4>
                                        <div  class="desc_product" >{{ $product->description }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Attach additional materials:</label>
                                        <div class="files_wrap" id="files_wrap">
                                            @if($product->files)
                                                @foreach($product->files as $file)
                                                    <a href="{{ asset($file) }}"  download class="file_block">
                                                        <div class="file_name_block">
                                                            <span class="file_name">{{ pathinfo($file)['basename'] }}</span>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        @if($product->status_deadline)
                        <div class="col-md-3">
                            <h3>Deadline</h3>
                            <div class="wizard_verticle">
                                <ul class="wizard_steps anchor status_list">
                                    @foreach( $statuses as $status)
                                        <li>
                                            <a  class="status_icon selected">
                                                <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                            </a>
                                            <div class="date_wrap">
                                                <div class="step_descr">
                                                    {{ $status->description }}<br>
                                                </div>
                                                <div class="status_date">
                                                    {{ $product->getDateStatus($product->status_deadline[$status->name]) }}
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif
                         @php $index = optional($product->productStatus)->index?: 0  @endphp
                        <div class="col-md-3">
                            <h3>Status Report</h3>
                            <div class="wizard_verticle">
                                <ul class="wizard_steps anchor status_list">
                                    @foreach( $statuses as $status)
                                        <li>
                                            <a  class="status_icon {{ $status->index <= $index? 'selected': 'disabled' }}">
                                                <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                            </a>
                                            <div class="date_wrap">
                                                <div class="step_descr">
                                                    @if($status->index === $index)
                                                    <strong>{{ $status->description }}</strong>
                                                    @else
                                                        {{ $status->description }}
                                                    @endif
                                                </div>
                                                <div class="status_date">
                                                    @if($product->status_report[$status->name])
                                                        @if($status->index === $index)
                                                            <strong>{{ $product->getDateStatus($product->status_report[$status->name]) }}</strong>
                                                        @else
                                                            {{ $product->getDateStatus($product->status_report[$status->name]) }}
                                                        @endif
                                                    @elseif(($index + 1) == $status->index)
                                                        <div class="step_completed"><i class="fa fa-check"></i> Mark this step completed.</div>
                                                        <form action="{{ route('update.status', $product->id) }}" method="post" class="update_status_form">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="status" value="{{ $status->name }}">
                                                            <fieldset>
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                                                                            <input
                                                                                    type="text"
                                                                                    class="form-control has-feedback-left calendar_status_up"
                                                                                    name="status_date"
                                                                                    value=""
                                                                            >
                                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                            <div class="button_wrap">
                                                                <button class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Save</button>
                                                            </div>
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

