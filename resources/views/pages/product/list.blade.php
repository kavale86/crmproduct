@extends('layouts.layouts')

@section('content')

    @if (session('success'))
        <div class="alert alert-success status_alert" id="status_alert">
            {{ session('success') }}
        </div>
    @endif
<div class="page-suppliers">
    <div class="page-title">
        <div class="title_left">
            <h3>{{__('List of Products')}}</h3>
        </div>
        <div class="title_right">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('products.index') }}" method="get">
                        <div class="form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" value="{{ $search }}" name="search" placeholder="Search for...">
                                <span class="input-group-btn">
                              <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="row">
                    <div class="col-md-6">
                        @if($products->isNotEmpty())
                        <div class="align-items-center">
                            @php
                                $count = $products->count();
                                $perPage = $products->perpage();
                                $currentPage = $products->currentPage();
                                $total = $products->total();
                                $showing = $currentPage == 1? 1: $perPage * ($currentPage-1);
                            @endphp
                            <span style="margin-right: 5px;"> Showing {{ $showing  }} - {{ $currentPage == 1? $count: $showing+$count }} of {{ $total }}  Products</span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <a href="{{route('products.create')}}" class="btn btn-sm btn-primary" id="add_supplier">{{__('Add New product')}}</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                @if($products->isNotEmpty())
                <!-- start project list -->
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th>Number:</th>
                            <th>Product:</th>
                            <th>Status:</th>
                            <th>Start:</th>
                            <th>Developer:</th>
                            <th>Client:</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ empty($product->status)? 'no-status': $product->productStatus->description }}</td>
                            <td>{{ empty($product->start_date)? '':$product->start_date->format('d.m.Y') }}</td>
                            <td>{{ $product->developer->name }}</td>
                            <td>{{ $product->client->name }}</td>
                            <td>
                                <div class="td_actions">
                                    <a href="{{ route('products.show',$product->id)}}" title="view" class="btn btn-success btn-xs" ><i class="fa fa-eye"></i></a>
                                    <a href="{{ route('products.edit',$product->id)}}" title="edit" class="btn btn-primary btn-xs edit_post" ><i class="fa fa-pencil"></i></a>
                                    <form action="{{ route('products.destroy', $product->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-xs delete_post"><i class="fa fa-trash-o"></i> </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $products->links() }}
                    <!-- end project list -->
                    @else
                        <div>Nothing found</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
