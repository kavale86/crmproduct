@extends('layouts.layouts')

@section('content')

    @if (session('success'))
        <script>
            show_modal = true;
        </script>
    @else
        <script>
            show_modal = false;
        </script>
    @endif
    <div class="row">
        <div class="col-md-10">
            <div>
                <h4>{{ __('Add New Product') }}</h4>
            </div>
            <form id="create-product-form" action="{{ route('products.store') }}" method="POST" >
                {{ csrf_field() }}
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-8 col_border_right">
                                <h6>Product Details</h6>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Name * </label>
                                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" />
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Developer * </label>
                                            <select name="developer_id" value=""  class="form-control">
                                                @foreach($developers as $developer)
                                                    <option {{ old('developer_id')==$developer->id? 'selected': '' }}  value="{{ $developer->id }}">{{ $developer->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Client * </label>
                                            <select name="client_id" value="" class="form-control">
                                                @foreach($clients as $client)
                                                    <option {{ old('client_id')==$client->id? 'selected': '' }}  value="{{ $client->id }}">{{ $client->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description(20 chars min):</label>
                                            <textarea name="description" class="form-control"  rows="15"  placeholder="Description of good" >{{ old('description') }}</textarea>
                                            <span class="text-danger">{{ $errors->first('description') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>Attach additional materials:</label>
                                            <div class="files_wrap" id="files_wrap">

                                                <input type="file" id="file_product">

                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                @can('superAdmin')
                                <div class="wizard_verticle">
                                    <ul class="wizard_steps anchor status_list">
                                        @foreach( $statuses as $status)
                                            <li>
                                                <a  class="status_icon">
                                                    <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                                </a>
                                                <div class="date_wrap">
                                                    <div class="step_descr">
                                                        {{ $status->description }}<br>
                                                    </div>
                                                    <div class="status_date">
                                                        <fieldset>
                                                            <div class="control-group">
                                                                <div class="controls">
                                                                    <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                                                                        <input
                                                                                type="text"
                                                                                class="form-control has-feedback-left calendar_status_up"
                                                                                name="statuses[{{ $status->name }}]"
                                                                                value="{{ empty($product->status_deadline[$status->name])? '': $product->getDateStatus($status->name) }}"
                                                                        >
                                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endcan
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="button_block">
                                    <a href="{{ route('products.index') }}" class="btn btn-danger">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                                <div class="alert_block_form" id="alert_block_form"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('common.modals.product_add_modal')

@endsection

