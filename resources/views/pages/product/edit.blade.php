@extends('layouts.layouts')

@section('content')

    @if (session('success'))
        <script>
            show_modal = true;
        </script>
    @else
        <script>
            show_modal = false;
        </script>
    @endif
    @php $index = optional($product->productStatus)->index?: 0  @endphp
    <div>
        <h4>{{ __('Update Product') }}</h4>
    </div>
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-md-9">
                        <form id="create-product-form" action="{{ route('products.update', $product->id) }}" method="POST" >
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="row">
                                <div class="col-md-8 col_border_right">
                                    <h6>Product Details</h6>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name">Name * </label>
                                                <input type="text" id="name" name="name" class="form-control" value="{{ $product->name }}" />
                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Developer * </label>
                                                <select name="developer_id" value=""  class="form-control">
                                                    @foreach($developers as $developer)
                                                        <option {{ $product->developer_id == $developer->id? 'selected': '' }}  value="{{ $developer->id }}">{{ $developer->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Client * </label>
                                                <select name="client_id" value="" class="form-control">
                                                    @foreach($clients as $client)
                                                        <option {{ $product->client_id == $client->id? 'selected': '' }}  value="{{ $client->id }}">{{ $client->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description(20 chars min):</label>
                                                <textarea name="description" class="form-control"  rows="15"  placeholder="Description of good" >{{ $product->description }}</textarea>
                                                <span class="text-danger">{{ $errors->first('description') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label>Attach additional materials:</label>
                                                <div class="files_wrap" id="files_wrap">
                                                    @if($product->files)
                                                        @foreach($product->files as $file)
                                                            <div class="file_block">
                                                                <input type="hidden"  name="files[]" value="{{ $file }}" class="input_file">
                                                                <div class="file_name_block">
                                                                    <span class="file_name">{{ pathinfo($file)['basename'] }}</span>
                                                                    <span class="delete_file edit"><i class="fa fa-remove"></i></span>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                    <input type="file" id="file_product">

                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                                @can('superAdmin')
                                    <div class="col-md-4">
                                        <h5>Status Deadline</h5>
                                        <div class="wizard_verticle">
                                            <ul class="wizard_steps anchor status_list">
                                                @foreach( $statuses as $status)
                                                    <li>
                                                        <a  class="status_icon">
                                                            <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                                        </a>
                                                        <div class="date_wrap">
                                                            <div class="step_descr">
                                                                {{ $status->description }}<br>
                                                            </div>
                                                            <div class="status_date">
                                                                <fieldset>
                                                                    <div class="control-group">
                                                                        <div class="controls">
                                                                            <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                                                                                <input
                                                                                        type="text"
                                                                                        class="form-control has-feedback-left calendar_status_up"
                                                                                        name="statuses[{{ $status->name }}]"
                                                                                        value="{{ empty($product->status_deadline[$status->name])? '': $product->getDateStatus($product->status_deadline[$status->name]) }}"
                                                                                >
                                                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endcan

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="button_block">
                                        <a href="{{ route('products.index') }}" class="btn btn-danger">Cancel</a>
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                    <div class="alert_block_form" id="alert_block_form"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <h5>Status Report</h5>
                        <div class="wizard_verticle">
                            <ul class="wizard_steps anchor status_list">
                                @foreach( $statuses as $status)
                                    <li>
                                        <a  class="status_icon {{ $status->index <= $index? 'selected': 'disabled' }}">
                                            <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                        </a>
                                        <div class="date_wrap">
                                            <div class="step_descr">
                                                @if($status->index === $index)
                                                    <strong>{{ $status->description }}</strong>
                                                @else
                                                    {{ $status->description }}
                                                @endif
                                            </div>
                                            <div class="status_date">
                                                @if($product->status_report[$status->name])
                                                    @if($status->index === $index)
                                                        <strong>{{ $product->getDateStatus($product->status_report[$status->name]) }}</strong>
                                                    @else
                                                        {{ $product->getDateStatus($product->status_report[$status->name]) }}
                                                    @endif
                                                @elseif(($index + 1) == $status->index)
                                                    <div class="step_completed"><i class="fa fa-check"></i> Mark this step completed.</div>
                                                    <form action="{{ route('update.status', $product->id) }}" method="post" class="update_status_form">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="status" value="{{ $status->name }}">
                                                        <fieldset>
                                                            <div class="control-group">
                                                                <div class="controls">
                                                                    <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                                                                        <input
                                                                                type="text"
                                                                                class="form-control has-feedback-left calendar_status_up"
                                                                                name="status_date"
                                                                                value=""
                                                                        >
                                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div class="button_wrap">
                                                            <button class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Save</button>
                                                        </div>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>




    @include('common.modals.product_add_modal')

@endsection

