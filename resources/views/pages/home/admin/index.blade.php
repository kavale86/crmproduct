@extends('layouts.layouts')

@section('content')
    <div class="page-home">
        @include('common.home_top')

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Products</h2>
                        <div class="panel_toolbox">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{ route('search.home') }}" method="post" id="search_home">
                                        <div class="form-group pull-right top_search">
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="" name="search" placeholder="Search for...">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit">Go!</button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <form action="{{ route('filter.status') }}" method="get">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <select name="filter_status" class="form-control" id="filter_status">
                                                    <option value="all">All</option>
                                                    @foreach($statuses as $status)
                                                        <option value="{{ $status->name }}">{{ $status->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="products_wrap">
                        @if($products->isNotEmpty())
                            <div class="orders_list" id="products_list">
                                @foreach( $products as $product)
                                    <div class="row product_block x_title">
                                        <div class="col-md-3">
                                            <h4> {{ $product->client->name }} - {{ $product->name }}</h4>
                                            <ul class="detail_order">
                                                <li>
                                                    <span class="label">Product Number: </span>
                                                    <span class="value"><strong>{{ $product->id }}</strong></span>
                                                </li>
                                                <li>
                                                    <span class="label">Status: </span>
                                                    <span class="value"><strong>{{ empty($product->status)? 'no-status': $product->productStatus->description }}</strong></span>
                                                </li>
                                                <li>
                                                    <span class="label">Start: </span>
                                                    <span class="value"><strong>{{ empty($product->start_date)? '':$product->start_date->format('d.m.Y') }}</strong></span>
                                                </li>
                                                <li>
                                                    <span class="label">Product: </span>
                                                    <span class="value"><strong>{{ $product->name }}</strong></span>
                                                </li>
                                                <li>
                                                    <span class="label">Developer: </span>
                                                    <span class="value"><strong>{{ $product->developer->name }}</strong></span>
                                                </li>
                                                <li>
                                                    <span class="label">Client: </span>
                                                    <span class="value"><strong>{{ $product->client->name }}</strong></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form_wizard wizard_horizontal">
                                                <ul class="wizard_steps anchor">
                                                    @php $width = round(100/count($statuses)) @endphp
                                                    @php $index = optional($product->productStatus)->index?: 0  @endphp
                                                    @foreach( $statuses as $status)
                                                        <li style="width: {{$width}}%;">
                                                            <a  class="{{ $status->index <= $index? 'selected': 'disabled' }}">
                                                                <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                                                <span class="step_descr">
                                                {{ $status->description }}<br>
                                                <small>{{ !empty($product->status_report[$status->name])? $product->getDateStatus($product->status_report[$status->name]) :'' }}</small>
                                                </span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if($products->lastPage() > 1)
                                <button id="loar_products" data-lastpage="{{ $products->lastPage() }}" data-perpage="{{ $products->perPage() }}" class="btn btn-secondary">Load products</button>
                            @endif
                        @else
                            <div>Nothing found</div>
                        @endif
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection
