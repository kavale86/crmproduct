<div class="kpi_top">
    <a href="#" class="arrow arrow-left" data-timestamp="{{ $weeks['end_day']->timestamp }}" data-kpi="{{ $kpi }}">
        <i class="fa fa-arrow-circle-o-left"></i>
    </a>
    <span class="text">{{ $weeks['end_day']->format('l, F d') }} - {{ $weeks['start_day']->format('l, F d, Y') }}</span>
    @if($weeks['start_day']->format('d.m.Y') != now()->format('d.m.Y'))
        <a href="#" class="arrow arrow-left" data-timestamp="{{ $weeks['start_day']->addWeeks(5)->timestamp }}" data-kpi="{{ $kpi }}">
            <i class="fa fa-arrow-circle-o-right"></i>
        </a>
    @endif
</div>
<div class="kpi_table">
    <div class="item item_first">
        <div class="top_block">
            <div class="title">{{ \App\Models\Product::TITLES[$weeks['title']] }}</div>
        </div>
    </div>
    @foreach( $weeks['products'] as $key => $item)
        <div class="item">
            <div class="top_block">
                <div class="title">week {{ $key }}</div>
                <div class="text">{{ $item['start_date'] }} - {{ $item['end_date'] }}</div>
            </div>
            <div class="bottom_block">
                @if($item['result'] != 0)
                    <div class="count">{{ $item['result'] }}</div>
                @else
                    <div class="no_count"></div>
                @endif
            </div>
        </div>
    @endforeach
</div>
