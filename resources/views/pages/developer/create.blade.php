@extends('layouts.layouts')

@section('content')

    @if (session('success'))
        <script>
            show_modal = true;
        </script>
    @else
        <script>
            show_modal = false;
        </script>
    @endif
    <div class="row">
        <div class="col-md-6">
            <div>
                <h4>{{ __('Add New Developer') }}</h4>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h6>Details</h6>
                </div>
                <div class="x_content">
                    <form id="create-developer-form" action="{{ route('developers.store') }}" method="POST" style="max-width: 800px">
                        {{ csrf_field() }}
                        <input type="hidden" name="avatar" value="{{ old('avatar') }}" id="input_avatar">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="photo_wrapper">
                                    <div class="img_block text-center">
                                        <img id="img_avatar"
                                             class="img_avatar"
                                             src="{{ empty(old('avatar'))? asset('uploads/users/default_avatar.png'): old('avatar') }}"
                                             alt="avatar"
                                        >
                                    </div>
                                    <div class="button_block">
                                        <input type="file" id="file_upload" name="file" accept="image/jpeg,image/png" style="display: none">
                                        <label for="file_upload" id="button_file_upload" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></label>
                                        <button class="btn btn-secondary btn-sm"
                                                id="delete_file"
                                                type="button"
                                                data-default_avatar="{{ asset('uploads/users/default_avatar.png') }}"
                                        ><i class="fa fa-trash-o"></i></button>
                                    </div>
                                    <div id="error_block_file"></div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="name">Name * </label>
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" />
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email * </label>
                                    <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" />
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password * </label>
                                    <input type="password" id="password" class="form-control" name="password" />
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description(20 chars min):</label>
                                    <textarea name="description" class="form-control" placeholder="Description of good" >{{ old('description') }}</textarea>
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="button_block">
                                    <a href="{{ route('developers.index') }}" class="btn btn-danger">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                                <div class="alert_block_form" id="alert_block_form"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('common.modals.croppie_modal')
    @include('common.modals.size_modal')
    @include('common.modals.developer_add_modal')

@endsection

