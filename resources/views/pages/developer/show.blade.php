@extends('layouts.layouts')

@section('content')
    <div class="developer_page" id="developer_page" data-developer="{{ 'developer_'.$developer->id }}">
        @include('common.developer_top')

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Clients</h2>
                        <div class="pull-right">
                           @can('admin') <a href="{{ route('clients.create') }}" class="btn btn-primary">Add New Client</a> @endcan
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div id="products_wrap">
                            @if($clients->isNotEmpty())
                                <div class="clients_list" >
                                    <ul class="developers_clients_list">
                                        @foreach($clients as $client)
                                            <li class="client_wrap {{ 'tab_client_'.$client->id }}" data-class="{{ 'tab_client_'.$client->id }}">
                                                <div class="client_top_block">
                                                    <div class="title">{{ $client->client->name }}</div>
                                                    <div class="action_block td_actions">
                                                        @can('admin')
                                                        <a href="{{ route('clients.edit',$client->client->id)}}" class="btn btn-primary btn-sm edit_post" ><i class="fa fa-pencil"></i></a>
                                                        @endcan
                                                        <button class="caret_button"><i class="fa fa-caret-down"></i></button>
                                                    </div>
                                                </div>
                                                @if(!empty($products[$client->client_id]))
                                                    <ul class="products_list">
                                                        @foreach($products[$client->client_id] as $product)
                                                            <li class="product_wrap client_wrap {{ 'tab_product_'.$product->id }}" data-class="{{ 'tab_product_'.$product->id }}">
                                                                <div class="product_top_block client_top_block">
                                                                    <div class="title">{{ $product->name }}</div>
                                                                    <div class="action_block td_actions">
                                                                        <a href="{{ route('products.show',$product->id)}}" title="view" class="btn btn-success btn-sm" ><i class="fa fa-eye"></i></a>
                                                                        @can('admin')
                                                                        <a href="{{ route('products.edit',$product->id)}}" title="edit" class="btn btn-primary btn-sm edit_post" ><i class="fa fa-pencil"></i></a>
                                                                        <form action="{{ route('products.destroy', $product->id)}}" method="post" class="form_delete_product">
                                                                            {{ csrf_field() }}
                                                                            @method('DELETE')
                                                                            <button class="btn btn-danger btn-sm delete_post"><i class="fa fa-trash-o"></i> </button>
                                                                        </form>
                                                                        @endcan
                                                                        <button class="caret_button"><i class="fa fa-caret-down"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="row product_block x_title">
                                                                    <div class="col-md-3">
                                                                        <h4> {{ $product->client->name }} - {{ $product->name }}</h4>
                                                                        <ul class="detail_order">
                                                                            <li>
                                                                                <span class="label">Product Number: </span>
                                                                                <span class="value"><strong>{{ $product->id }}</strong></span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="label">Status: </span>
                                                                                <span class="value"><strong>{{ empty($product->status)? 'no-status': $product->productStatus->description }}</strong></span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="label">Start: </span>
                                                                                <span class="value"><strong>{{ $product->created_at->format('d.m.Y') }}</strong></span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="label">Product: </span>
                                                                                <span class="value"><strong>{{ $product->name }}</strong></span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="label">Developer: </span>
                                                                                <span class="value"><strong>{{ $product->developer->name }}</strong></span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="label">Client: </span>
                                                                                <span class="value"><strong>{{ $product->client->name }}</strong></span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <div class="form_wizard wizard_horizontal">
                                                                            <ul class="wizard_steps anchor">
                                                                                @php $width = round(100/count($statuses)) @endphp
                                                                                @php $index = optional($product->productStatus)->index?: 0  @endphp
                                                                                @foreach( $statuses as $status)
                                                                                    <li style="width: {{$width}}%;">
                                                                                        <a  class="{{ $status->index <= $index? 'selected': 'disabled' }}">
                                                                                            <span class="step_no"><i class="fa {{ $status->icon }}" ></i></span>
                                                                                            <span class="step_descr">
                                                {{ $status->description }}<br>
                                                <small>{{ !empty($product->status_report[$status->name])? $product->getDateStatus($product->status_report[$status->name]) :'' }}</small>
                                                </span>
                                                                                        </a>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>

                                </div>
                                {{ $clients->links() }}
                            @else
                                <div>Nothing found</div>
                            @endif
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
@endsection
