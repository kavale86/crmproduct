@extends('layouts.layouts')

@section('content')

    @if (session('success'))
        <div class="alert alert-success status_alert" id="status_alert">
            {{ session('success') }}
        </div>
    @endif
<div class="page-suppliers">
    <div class="page-title">
        <div class="title_left">
            <h3>{{__('List of Developers')}}</h3>
        </div>
        <div class="title_right">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('developers.index') }}" method="get">
                        <div class="form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" value="{{ $search }}" name="search" placeholder="Search for...">
                                <span class="input-group-btn">
                              <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="row">
                    <div class="col-md-6">
                        @if($developers->isNotEmpty())
                        <div class="align-items-center">
                            @php
                                $count = $developers->count();
                                $perPage = $developers->perpage();
                                $currentPage = $developers->currentPage();
                                $total = $developers->total();
                                $showing = $currentPage == 1? 1: $perPage * ($currentPage-1);
                            @endphp
                            <span style="margin-right: 5px;"> Showing {{ $showing  }} - {{ $currentPage == 1? $count: $showing+$count }} of {{ $total }}  Developers</span>
                        </div>
                         @endif
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('developers.create')}}" class="btn btn-sm btn-primary pull-right" id="add_supplier">{{__('Add New Developer')}}</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                @if($developers->isNotEmpty())
                <!-- start project list -->
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 15%;" >Avatar</th>
                            <th>Name</th>
                            <th>Email</th>

                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($developers as $developer)
                        <tr>
                            <td><img src="{{ isset($developer->avatar)? asset($developer->avatar) : asset('uploads/users/default_avatar.png') }}" alt="" style="width: 60px;"></td>
                            <td>{{$developer->name}}</td>
                            <td>{{$developer->email}}</td>
                            <td>
                                <div class="td_actions">
                                    <a href="{{ route('developers.show', $developer->id) }}" class="btn btn-success btn-xs">
                                        <i class="fa fa-eye"></i>
                                         Developer page
                                    </a>
                                    <a href="{{ route('developers.edit',$developer->id)}}" class="btn btn-primary btn-xs edit_post" ><i class="fa fa-pencil"></i></a>
                                    <form action="{{ route('developers.destroy', $developer->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-xs delete_post"><i class="fa fa-trash-o"></i> </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                {{ $developers->links() }}
                    <!-- end project list -->
                    @else
                        <div>Nothing found</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
