$(document).ready(function () {

    // Avatar Croppie
    let reader;
    let croppie_img;
    let input;
    $('#file_upload').on('change', function () {
        reader = new FileReader();
        input = $(this);
        if(input[0].files[0].size > 10485760){
            $.magnificPopup.open({
                items: {
                    src: '#size_modal',
                    type: 'inline'
                }
            });
            input.val('');
            return false;
        }
        reader.onload = function (e) {
            $.magnificPopup.open({
                items: {
                    src: '#croppie_modal',
                    type: 'inline',
                    removalDelay: 300,
                    mainClass: 'mfp-fade'
                },
                callbacks: {
                    open: function() {
                        croppie_img = $('#croppie_img').croppie({
                            viewport: {
                                width: 200,
                                height: 200,
                            },
                            boundary: { width: 300, height: 300 },
                            enableOrientation: true,
                        });
                        croppie_img.croppie('bind',{
                            url: e.target.result,
                        });
                    },
                    close: function() {
                        input.val('');
                        croppie_img.croppie('destroy');
                    }
                }
            });
        };
        reader.readAsDataURL(input[0].files[0])
    });
    $('#close_croppie').on('click touchdown', function (e) {
        e.preventDefault();
        croppie_img.croppie('result', {
            type: 'base64',
            size: 'viewport',
            format: 'png'
        }).then(function(canvas) {
            $('#button_file_upload').addClass('disabled').attr('for','');
            $('#input_avatar').val(canvas);
            $('#img_avatar').attr('src', canvas);
            input.val('');
        });
        croppie_img.croppie('destroy');
        $.magnificPopup.close();
    });
    $('#delete_file').on('click', function () {
        default_avatar = $(this).data('default_avatar');
        $('#input_avatar').val('');
        $('#img_avatar').attr('src', default_avatar);
        $('#button_file_upload').removeClass('disabled').attr('for','file_upload');
        if ($(this).hasClass('update_user')) {
            $('#del_avatar').val('delete_avatar');
        }
    });
    // End Avatar Croppie

    // Status alert
    if($('#status_alert').length != 0){
        $('#status_alert').slideDown('300');
        setTimeout(function () {
            $('#status_alert').slideUp('300');
        }, 4000)
    }
    if(show_modal != undefined && show_modal){
        $.magnificPopup.open({
            items: {
                src: '#user_add_modal',
                type: 'inline',
                removalDelay: 300,
                mainClass: 'mfp-fade'
            },
        });
    }

    //add files product
    function add_file(name){
        return '<div class="file_block">' +
            '<span class="file_name">'+ name +'</span> <span class="delete_file"></span>' +
            '</div>';
    }
    $('#file_product').on('change', function () {
        $this = $(this);
        $file = $this[0].files[0];
        formData = new FormData();
        formData.append('file', $file);
        $.ajax({
            url: '/uploads/product/file',
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if(data.success){
                    $this.val('');
                    $('#files_wrap').prepend(data.html);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $(document).on('click', '.delete_file', function () {
        $this = $(this);
        $file = $this.parents('.file_block').children('.input_file').val();
        console.log($file);
        if($this.hasClass('edit')){
            $this.parents('.file_block').remove();
        }else{
            $.ajax({
                url: '/delete/product/file',
                data: {'file': $file},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if(data.success){
                        $this.parents('.file_block').remove();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

    //datatables

    $('#sorttable').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     false,
        "searching": false
    } );

    // load products home page
    let home_page = 1;
    $(document).on('click', '#loar_products' , function () {
        $this = $(this);
        perpage = $this.data('perpage');
        lastpage = $this.data('lastpage');
        search = $('#search_home').val();
        filter = $('#filter_status').val();
        offset = +perpage * home_page;
        data = {'offset': offset, 'limit': perpage}
        if(search != ''){
            data.search = search;
        }
        if(filter != 'all'){
            data.filter = filter;
        }
        $.ajax({
            url: '/load_products',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                $('#products_list').append(data.html);
                home_page ++;
                if(home_page == lastpage){
                    $this.remove();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    // filters dashboard
    if($('#filter_status').length != 0){
        $('#filter_status').on('change', function () {
            url_filter = $(this).parents('form').attr('action');
            status_filter = $(this).val();
            $.ajax({
                url: url_filter,
                data: {'status': status_filter},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('#products_wrap').html(data.html);
                    home_page = 1;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    }

    // search dashboard
    $('#search_home').submit(function (e) {
        e.preventDefault();
        url_search = $(this).attr('action');
        search = $(this).find('input').val();
        $.ajax({
            url: url_search,
            data: {'search': search},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('#products_wrap').html(data.html);
                home_page = 1;
            },
            error: function (error) {
                console.log(error);
            }
        });
        return false;
    });

    //delete file
    $(document).on('click', '.del_file', function () {
        order_id = $('#order_detail').data('id');
        path_file = $(this).data('path');
        name_file = $(this).data('name');
        parent_block = $(this).parent('.form-group');
        $.ajax({
            url: '/delete_file',
            data: {'order_id':order_id, 'name_file': name_file, 'path_file': path_file},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if(data.status == 'ok'){
                    parent_block.remove();
                    $('#success_wrap').html('<div class="alert alert-success status_alert" id="status_alert">\n' +
                        '            File delete successfully \n' +
                        '        </div>');
                    $('#status_alert').slideDown('300');
                    setTimeout(function () {
                        $('#status_alert').slideUp('300');
                    }, 4000)
                }
            },
            error: function (error) {
                console.log(error);
            }
        });

    });

    //delete offers home
    $('.delete_offer_home').on('submit', function (e) {
        e.preventDefault();
        url = $(this).attr('action');
        $.ajax({
            url: url,
            data: '',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                if(data.status == 'ok'){
                    location.reload();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });

        return false;
    });

    //mark completed
    $('.step_completed').on('click', function () {
        $(this).addClass('hide');
        $(this).parents('.status_date').find('.update_status_form').addClass('active');
    });

    // developer page toggle block
    let  tabs_client = {};
    if($('#developer_page').length !=0){
        let developer = $('#developer_page').data('developer');
        if(sessionStorage.getItem('tabs_client')){
            tabs_client = JSON.parse(sessionStorage.getItem('tabs_client'));
            if(tabs_client[developer]){
                for(i=0; i<tabs_client[developer].length;i++){
                    $('.'+tabs_client[developer][i]).addClass('active');
                }
            }
        }
        $('.client_top_block').on('click', function (e) {
            if($(e.toElement).hasClass('client_top_block') || $(e.toElement).hasClass('fa-caret-down')){
                item_class = $(this).parent('.client_wrap').data('class');
                if($(this).parent('.client_wrap').hasClass('active')){
                    $(this).parent('.client_wrap').removeClass('active');
                    if(tabs_client[developer]){
                        tabs_client[developer].pop(item_class);
                        sessionStorage.setItem('tabs_client', JSON.stringify(tabs_client));
                    }
                } else {
                    $(this).parent('.client_wrap').addClass('active');
                    if(tabs_client[developer]){
                        tabs_client[developer].push(item_class);
                        sessionStorage.setItem('tabs_client', JSON.stringify(tabs_client));
                    } else {
                        tabs_client[developer] = [item_class];
                        sessionStorage.setItem('tabs_client', JSON.stringify(tabs_client));
                    }
                }
            }

        });
    }



    // perpage form
    $('#select_perpage').on('change', function () {
        $('#perpage_form').submit();
    });


    //date status
    $('.calendar_status').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD.M.Y'
        }
    });

    $('.calendar_status_up').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: 'up',
        locale: {
            format: 'DD.M.Y'
        }
    });

    $('.calendar_status_down').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: 'down',
        locale: {
            format: 'DD.M.Y'
        }
    });

    //close popup
    $('#close_popup').on('click', function () {
        $.magnificPopup.close();
    });

    //kpi ajax
    $(document).on('click', '.kpi_top .arrow', function (e) {
        e.preventDefault();
        timestamp = $(this).data('timestamp');
        kpi = $(this).data('kpi');
        $.ajax({
            url: '/kpi',
            data: { 'kpi': kpi, 'timestamp': timestamp},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $('#kpi_wrapper').html(data);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('.form_delete_product').on('submit', function (e) {
        e.preventDefault();

        $this = $(this);
        url = $this.attr('action');
        $.ajax({
            url: url,
            data: '',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                if(data.status == 'ok'){
                    $this.parents('.product_wrap').remove();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
        return false;
    })


});
