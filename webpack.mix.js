const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* styles */

mix.styles([
    'resources/gentelella/bootstrap/dist/css/bootstrap.min.css',
    'resources/gentelella/font-awesome-4.7.0/css/font-awesome.min.css',
    'resources/gentelella/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
    'resources/gentelella/Magnific-Popup-master/dist/magnific-popup.css',
    'resources/gentelella/bootstrap-daterangepicker/daterangepicker.css',
    'resources/gentelella/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
    'resources/gentelella/datatables.net-bs/css/dataTables.bootstrap.min.css',
    // 'resources/gentelella/nprogress/nprogress.css',
    // 'resources/gentelella/dropzone/dist/min/dropzone.min.css',
    'resources/gentelella/croppie/croppie.css',
    'resources/gentelella/custom.min.css',
], 'public/css/gentelella.css');

mix.sass('resources/sass/style.scss', 'public/css');

/* scripts */

mix.scripts([
    'resources/gentelella/jquery/dist/jquery.min.js',
    'resources/gentelella/moment/min/moment.min.js',
    'resources/gentelella/bootstrap/dist/js/bootstrap.bundle.min.js',
    'resources/gentelella/bootstrap-progressbar/bootstrap-progressbar.min.js',
    'resources/gentelella/bootstrap-daterangepicker/daterangepicker.js',
    'resources/gentelella/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    'resources/gentelella/datatables.net/js/jquery.dataTables.min.js',
    'resources/gentelella/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js',
    'resources/gentelella/iCheck/icheck.min.js',
    'resources/gentelella/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',
    'resources/gentelella/croppie/croppie.min.js',
    // 'resources/gentelella/nprogress/nprogress.js',
    // 'resources/gentelella/fastclick/lib/fastclick.js',
    // 'resources/gentelella/dropzone/dist/min/dropzone.min.js',
    'resources/gentelella/custom.min.js',
], 'public/js/gentelella.js');

mix.scripts([
    'resources/js/script.js'
], 'public/js/script.js');

// mix.scripts([
//     'resources/js/maps.js'
// ], 'public/js/maps.js');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
