<?php


namespace App\Services\Upload;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class UploadFileService
{
    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadAvatar(Request $request){
        $data = explode(',', $request->input('avatar'));
        $file = null;
        if (isset($data[1])) {
            $file = base64_decode($data[1]);
        }
        $pathFile = 'users/'.Str::random(30).'.png';
        $newFile = Storage::disk('public')->put( $pathFile, $file);

        if($newFile){
            return 'uploads/'.$pathFile;
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function uploadAvatarClient(Request $request){
        $data = explode(',', $request->input('avatar'));
        $file = null;
        if (isset($data[1])) {
            $file = base64_decode($data[1]);
        }
        $pathFile = 'clients/'.Str::random(30).'.png';
        $newFile = Storage::disk('public')->put( $pathFile, $file);

        if($newFile){
            return 'uploads/'.$pathFile;
        }
    }

    public function uploadProductFile(Request $request){

    }

    /**
     * @param $file
     */
    public function deleteFile($file)
    {
        $oldFile = public_path($file);
        if (file_exists($oldFile)) {
            \File::delete($oldFile);
        }
    }

}
