<?php
/**
 * Created by PhpStorm.
 * User: mihailkovalev
 * Date: 2/24/20
 * Time: 1:43 PM
 */

namespace App\Services;


use App\Models\Product;
use App\Models\Status;
use Carbon\CarbonImmutable;

class KpiServices
{
    /**
     * @param $products
     * @return float|int
     */
    public function averageTimeHome($products){
        $count = count($products);
        $days = 0;
        foreach ($products as $product){
            $status_report = $product->status_report;
            $average_milliseconds = $status_report[Status::GOODS] - $status_report[Status::CONFIRM];
            $average_days = ceil($average_milliseconds / (24*60*60));
            $days += $average_days;
        }

        return $days == 0? 0: floor($days/$count);
    }

    /**
     * @param $products
     * @return array
     */
    public function developerTime($products){
        $count = count($products);
        $days = 0;
        $days_60 = [];
        $days_7 = [];
        $days_14 = [];
        foreach ($products as $product){
            $status_report = $product->status_report;
            $average_milliseconds = $status_report[Status::GOODS] - $status_report[Status::CONFIRM];
            $average_days = ceil($average_milliseconds / (24*60*60));
            if($average_days < 60 ){
                $days_60[] = 1;
            }
            if($average_days > 7  and $average_days < 14){
                $days_7[] = 1;
            }
            if($average_days > 14 ){
                $days_14[] = 1;
            }
            $days += $average_days;
        }

        $arrayTime = [
            'average_time' => $days == 0? 0: floor($days/$count),
            'in_time' => empty($days_60)? 0: floor((count($days_60)/$count)*100),
            'critical' => empty($days_7)? 0: floor((count($days_7)/$count)*100),
            'overtime' => empty($days_14)? 0: floor((count($days_14)/$count)*100)
        ];
        return $arrayTime;
    }

    /**
     * @return array
     */
    public function getWeekProductCount($start, $title){
        $start = CarbonImmutable::now()->timestamp($start);
        $startDay = $start->format('Y-m-d');
        $endDay = $start->addWeeks(-5)->format('Y-m-d');
        if($title != Product::WEEK_COUNT){
            $products = Product::where('status', '=', Status::GOODS)
                ->whereDate('created_at','>=', $endDay)
                ->whereDate('created_at','<=', $startDay)
                ->get();
        } else {
            $products = Product::whereDate('created_at','>=', $endDay)
                ->whereDate('created_at','<=', $startDay)
                ->get();
        }

        $arrayProducts = [];
        for($i=-5;$i <= 0; $i++){
            $week = $start->addWeeks($i);
            $arrayProducts[$week->week] = [
                'start_date' => $week->startOfWeek()->format('l, F d'),
                'end_date' => $week->endOfWeek()->format('l, F d, Y'),
                'items' => []
            ];
        }
        if($products->isNotEmpty()){
            foreach ( $products as $product){
                $arrayProducts[$product->created_at->week]['items'][] = $product;
            }
        }
        switch($title){
            case Product::WEEK_COUNT :
                foreach ($arrayProducts as $key => $item){
                    $arrayProducts[$key]['result'] = count($item['items']);
                }
                break;
            case Product::WEEK_AVERAGE :
                foreach ($arrayProducts as $key => $item){
                    $days = $this->averageTime($item['items']);
                    $arrayProducts[$key]['result'] = $days;
                }
                break;
            case Product::WEEK_CRITICAL :
                foreach ($arrayProducts as $key => $item){
                    $days = $this->critical($item['items']);
                    $arrayProducts[$key]['result'] = $days;
                }
                break;
            case Product::WEEK_TIME :
                foreach ($arrayProducts as $key => $item){
                    $days = $this->inTime($item['items']);
                    $arrayProducts[$key]['result'] = $days;
                }
                break;
            case Product::WEEK_OVERTIME :
                foreach ($arrayProducts as $key => $item){
                    $days = $this->overtime($item['items']);
                    $arrayProducts[$key]['result'] = $days;
                }
                break;
        }
        $result = [
            'start_day' => $start,
            'end_day' => $start->addWeeks(-5),
            'title' => $title,
            'products' => $arrayProducts
        ];

        return $result;
    }

    /**
     * @param $products
     * @return float|int
     */
    private function averageTime($products){
        $count = count($products);
        $days = 0;
        foreach ($products as $product){
            $status_report = $product->status_report;
            $average_milliseconds = $status_report[Status::GOODS] - $status_report[Status::CONFIRM];
            $average_days = ceil($average_milliseconds / (24*60*60));
            $days += $average_days;
        }

        return $days == 0? 0: floor($days/$count);
    }

    /**
     * @param $products
     * @return float|int
     */
    private function critical($products){
        $count = count($products);
        $days_7 = [];
        foreach ($products as $product){
            $status_report = $product->status_report;
            $average_milliseconds = $status_report[Status::GOODS] - $status_report[Status::CONFIRM];
            $average_days = ceil($average_milliseconds / (24*60*60));
            if($average_days > 7 and $average_days < 14 ){
                $days_7[] = 1;
            }
        }
        $result = empty($days_7)? 0: floor((count($days_7)/$count)*100);
        return $result;
    }

    /**
     * @param $products
     * @return float|int
     */
    private function inTime($products){
        $count = count($products);
        $days_60 = [];
        foreach ($products as $product){
            $status_report = $product->status_report;
            $average_milliseconds = $status_report[Status::GOODS] - $status_report[Status::CONFIRM];
            $average_days = ceil($average_milliseconds / (24*60*60));
            if($average_days < 60 ){
                $days_60[] = 1;
            }
        }
        $result = empty($days_60)? 0: floor((count($days_60)/$count)*100);
        return $result;
    }

    /**
     * @param $products
     * @return float|int
     */
    private function overtime($products){
        $count = count($products);
        $days_14 = [];
        foreach ($products as $product){
            $status_report = $product->status_report;
            $average_milliseconds = $status_report[Status::GOODS] - $status_report[Status::CONFIRM];
            $average_days = ceil($average_milliseconds / (24*60*60));
            if($average_days > 14 ){
                $days_14[] = 1;
            }
        }
        $result = empty($days_14)? 0: floor((count($days_14)/$count)*100);
        return $result;
    }

}
