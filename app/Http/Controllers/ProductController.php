<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Client;
use App\Models\Product;
use App\Models\Status;
use App\Models\User;
use App\Services\Upload\UploadFileService;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProductController extends Controller
{

    private $service;

    public function __construct(UploadFileService $service)
    {
        $this->middleware('checkAdmin')->only(['index', 'create', 'update', 'store','destroy','edit']);
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = '';
        if ($request->filled('search')){
            $search = $request->input('search');
            $products = Product::where('name', 'like', '%'.$search.'%')
                ->latest()
                ->paginate(15);
        } else {
            $products = Product::latest()
                ->paginate(15);
        }
        return view('pages.product.list', compact('products', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $developers = User::where('role', '=', User::ROLE_DEVELOPER)->get();
        $statuses = Status::all();
        return view('pages.product.create', compact('clients', 'developers', 'statuses'));
    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'developer_id' => $request->input('developer_id'),
            'client_id' => $request->input('client_id'),
            'description' => $request->input('description'),
        ];
        if($request->has('files')){
            $data['files'] = $request->input('files');
        }
        $arrayStatusesDeadline = [];
        $arrayStatusesReport = [];
        if($request->filled('statuses')){
            foreach ($request->input('statuses') as $key=>$value){
                $arrayStatusesDeadline[$key] = Carbon::parse($value)->timestamp;
                $arrayStatusesReport[$key] = null;
            }
            $data['status_deadline'] = $arrayStatusesDeadline;
            $data['status_report'] = $arrayStatusesReport;
        }else{
            $statuses = Status::all();
            foreach ($statuses as $status){
                $arrayStatusesReport[$status->name] = null;
            }
            $data['status_report'] = $arrayStatusesReport;
        }


        $product = Product::create($data);
        if($product){
            return back()
                ->with('success',['text'=>'Product Successfully Added!', 'id'=>$product->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $statuses = Status::all();
        $product = Product::find($id);
        return view('pages.product.show', compact('statuses', 'product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $clients = Client::all();
        $developers = User::where('role', '=', User::ROLE_DEVELOPER)->get();
        $statuses = Status::all();
        return view('pages.product.edit', compact('product', 'clients', 'developers', 'statuses'));
    }

    /**
     * @param ProductRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, $id)
    {
        $product = Product::find($id);
        $data = [
            'name' => $request->input('name'),
            'developer_id' => $request->input('developer_id'),
            'client_id' => $request->input('client_id'),
            'description' => $request->input('description'),
        ];

        $arrayStatusesDeadline = [];
        if($request->filled('statuses')){
            foreach ($request->input('statuses') as $key=>$value){
                $arrayStatusesDeadline[$key] = Carbon::parse($value)->timestamp;
            }
            $data['status_deadline'] = $arrayStatusesDeadline;
        }

        if($request->has('files')){
            if(!empty($product->files)){
                foreach ($product->files as $file){
                    if(!in_array($file, $request->input('files'))){
                        $this->service->deleteFile($file);
                    }
                }
            }
            $data['files'] = $request->input('files');

        } else {
            if(!empty($product->files)){
                foreach ($product->files as $file){
                    $this->service->deleteFile($file);
                }
            }
            $data['files'] = null;
        }
        $product->update($data);
        return back()
            ->with('success',['text'=>'Product Successfully Update!', 'id'=>$product->id]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $product = Product::find($id);
        $product->delete();
        if($request->ajax()){
            return response()->json(['status'=> 'ok']);
        }
        return redirect()->route('products.index')
            ->with('success', 'Product delete successfully!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function UploadFile(Request $request){
        $year = Carbon::now()->year;
        $pathProduct = 'products/'.$year;
        if($request->file('file')->isValid()){
            $name = $request->file('file')->getClientOriginalName();
            $path = 'uploads/'.$request->file('file')->storeAs($pathProduct, $name);
            $html = view('common.upload.file_product', compact('name', 'path'))->render();
            return response()->json(['success'=> true, 'html'=> $html]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request )
    {
        if($request->has('file')){
            $this->service->deleteFile($request->input('file'));
            return response()->json(['success'=> true]);
        }
    }

    public function updateStatus(Request $request, $id){

        $product = Product::find($id);
        $status = $request->input('status');
        $status_date = strtotime($request->input('status_date'));
        if($status == Status::CONFIRM){
            $product->start_date = $status_date;
        }
        $arrayStatuses = $product->status_report;
        $arrayStatuses[$status] = $status_date;
        $product->status_report = $arrayStatuses;
        $product->status = $status;
        $product->save();
        return back();
    }

}
