<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeveloperRequest;
use App\Http\Requests\DeveloperUpdateRequest;
use App\Models\Product;
use App\Models\Status;
use App\Models\User;
use App\Services\KpiServices;
use App\Services\Upload\UploadFileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DeveloperController extends Controller
{

    private $service;
    private $kpiService;
    public function __construct(UploadFileService $service, KpiServices $kpiService)
    {
        $this->middleware('checkAdmin')->only(['index', 'create', 'update', 'store','destroy','edit']);
        $this->service = $service;
        $this->kpiService = $kpiService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = '';
        if ($request->filled('search')){
            $search = $request->input('search');
            $developers = User::where([['name', 'like', '%'.$search.'%'], ['role', '=', User::ROLE_DEVELOPER]])
                ->latest()
                ->paginate(15);
        } else {
            $developers = User::where('role', '=', User::ROLE_DEVELOPER)
                ->latest()
                ->paginate(15);
        }


        return view('pages.developer.list', compact('developers', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.developer.create');
    }

    /**
     * @param DeveloperRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(DeveloperRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'role' => User::ROLE_DEVELOPER,
            'description' => $request->input('description'),
            'password' => Hash::make($request->input('password')),
        ];
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatar($request);
            $data['avatar'] = $avatar;
        }
        $developer = User::create($data);

        return back()
            ->with('success',['text'=>'Developer Successfully Added!', 'id'=>$developer->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        if($user->role == User::ROLE_ADMIN || $user->role == User::ROLE_SUPER_ADMIN || $user->id == $id ){
            $developer = User::find($id);
            $clients = Product::where('developer_id', '=', $developer->id)
                ->groupBy('client_id')
                ->paginate(2);
            $clients_ids = [];
            if($clients->isNotEmpty()){
                foreach ($clients as $client){
                    $clients_ids[] = $client->client_id;
                }
            }
            $items = Product::where('developer_id','=',$developer->id)
                ->whereIn('client_id',$clients_ids)
                ->get();
            $products = $items->groupBy('client_id');
            $count_products = Product::where('developer_id','=',$developer->id)->count();
            $products_goods = Product::where([['status', '=', Status::GOODS],['developer_id', '=', $developer->id]])->get();
            $arrayTime = $this->kpiService->developerTime($products_goods);
            $statuses = Status::all();

            return view('pages.developer.show', compact('clients', 'count_products', 'arrayTime', 'products', 'developer', 'statuses'));
        } else {
            return redirect()->route('home');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $developer = User::find($id);
        return view('pages.developer.edit', compact('developer'));
    }

    /**
     * @param DeveloperUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DeveloperUpdateRequest $request, $id)
    {
        $developer = User::find($id);
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'description' => $request->input('description'),
        ];

        if($request->filled('password') ){
            $data['password'] = Hash::make($request->input('password'));
        }
        if($request->filled('del_avatar')){
            $this->service->deleteFile($developer->avatar);
            $developer->avatar = null;
        }
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatar($request);
            $data['avatar'] = $avatar;
        }

        $developer->update($data);

        return back()
            ->with('success',['text'=>'Developer Successfully Added!', 'id'=>$developer->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $developer = User::find($id);
        $this->service->deleteFile($developer->avatar);
        $developer->delete();
        return redirect('developers')->with('success','Developer deleted successfully');
    }
}
