<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientCreateRequest;
use App\Models\Client;
use App\Services\Upload\UploadFileService;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    private $service;

    public function __construct(UploadFileService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = '';
        if ($request->filled('search')){
            $search = $request->input('search');
            $clients = Client::where('name', 'like', '%'.$search.'%')
                ->latest()
                ->paginate(15);
        } else {
            $clients = Client::latest()
                ->paginate(15);
        }
        return view('pages.client.list', compact('clients', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.client.create');
    }

    /**
     * @param ClientCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ClientCreateRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'description' => $request->input('description'),
        ];
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatarClient($request);
            $data['avatar'] = $avatar;
        }
        $client = Client::create($data);

        return back()
            ->with('success',['text'=>'Client Successfully Added!', 'id'=>$client->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('pages.client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'description' => $request->input('description'),
        ];

        if($request->filled('del_avatar')){
            $this->service->deleteFile($client->avatar);
            $client->avatar = null;
        }
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatar($request);
            $data['avatar'] = $avatar;
        }

        $client->update($data);

        return back()
            ->with('success',['text'=>'Client Successfully Added!', 'id'=>$client->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $this->service->deleteFile($client->avatar);
        $client->delete();

        return redirect('clients')->with('success','Client deleted successfully');
    }
}
