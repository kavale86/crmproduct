<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Status;
use App\Services\KpiServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KpiController extends Controller
{
    private $service;

    public function __construct(KpiServices $service)
    {
        $this->service = $service;
    }

    public function index(Request $request){
        $count_products = Product::count();
        $products_goods = Product::where([['status', '=', Status::GOODS]])->get();
        $arrayTime = $this->service->developerTime($products_goods);
        $kpi = Product::WEEK_COUNT;
        $start = $request->has('timestamp')? $request->input('timestamp') : Carbon::now()->timestamp;
        if($request->has('kpi')){
            $kpi = $request->input('kpi');
            $weeks = $this->service->getWeekProductCount($start, $request->input('kpi'));
        } else {
            $weeks = $this->service->getWeekProductCount($start, Product::WEEK_COUNT);
        }
        if($request->ajax()){
            $data = view('pages.kpi.kpi_block', compact( 'weeks', 'kpi'))->render();
            return response()->json($data);
        }
        return view('pages.kpi.index', compact('count_products', 'arrayTime', 'weeks', 'kpi'));
    }

    public function kpiAjax(Request $request){
        $kpi = $request->input('kpi');
        $start = $request->input('start');
    }

}
