<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\Upload\UploadFileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    private $service;

    public function __construct(UploadFileService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = User::find($id);
        return view('pages.user.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = User::find($id);
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'description' => $request->input('description'),
        ];

        if($request->filled('password') ){
            $data['password'] = Hash::make($request->input('password'));
        }
        if($request->filled('del_avatar')){
            $this->service->deleteFile($admin->avatar);
            $admin->avatar = null;
        }
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatar($request);
            $data['avatar'] = $avatar;
        }

        $admin->update($data);

        return back()
            ->with('success','Admin Successfully Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
