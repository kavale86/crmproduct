<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Status;
use App\Models\User;
use App\Services\KpiServices;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private $service;

    public function __construct(KpiServices $service)
    {
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if($user->isDeveloper()){
            $count_products = Product::where('developer_id','=',$user->id)->count();
            $products_goods = Product::where([['status', '=', Status::GOODS],['developer_id', '=', $user->id]])->get();
            $average_time = $products_goods->isNotEmpty()?$this->service->averageTimeHome($products_goods): 0;
            $products = Product::where('developer_id', '=', $user->id)->paginate(15);
        } else {
            $count_products = Product::count();
            $products_goods = Product::where('status', '=', Status::GOODS)->get();
            $average_time = $products_goods->isNotEmpty()?$this->service->averageTimeHome($products_goods): 0;
            $products = Product::paginate(15);
        }
        $statuses = Status::all();

        return view('pages.home.admin.index', compact('count_products', 'products', 'average_time', 'statuses'));
    }

    public function filterStatus(Request $request){
        $status = $request->input('status');
        $user = $request->user();
        $statuses = Status::all();
        if($user->role == User::ROLE_DEVELOPER){
            if($status == 'all'){
                $products = Product::where('developer_id','=', $user->id)
                    ->latest()
                    ->paginate(15);
            } else {
                $products = Product::where('status','=', $status)
                    ->where('developer_id','=', $user->id)
                    ->latest()
                    ->paginate(15);
            }
        } else {
            if($status == 'all'){
                $products = Product::latest()
                    ->paginate(15);
            } else {
                $products = Product::where('status','=', $status)
                    ->latest()
                    ->paginate(15);
            }

        }
        $html = view('common.product_home_block', compact(  'statuses', 'products'))->render();
        return response()->json(['html'=> $html]);
    }

    public function searchProduct(Request $request){
        $search = $request->input('search');
        $user = $request->user();
        $statuses = Status::all();
        if($user->role == User::ROLE_DEVELOPER){
            $products = Product::where('name','like', '%'.$search.'%')
                ->where('developer_id','=', $user->id)
                ->paginate(15);

        } else {
            $products = Product::where('name','like', '%'.$search.'%')
                ->paginate(15);
        }

        $html = view('common.product_home_block', compact(  'statuses', 'products'))->render();
        return response()->json(['html'=> $html]);
    }

    public function loadProducts(Request $request){
        $statuses = Status::all();
        $offset = (int)$request->input('offset');
        $limit = (int)$request->input('limit');
        $user = $request->user();
        $data = [];
        if($user->role == User::ROLE_DEVELOPER){
            $data[] = ['developer_id','=', $user->id];
        }
        if($request->has('filter')){
            $data[] = ['status','=', $request->input('filter')];
        }

        if($request->has('search')){
            $search = $request->input('search');
            $data[] = ['name','like', '%'.$search.'%'];
            $products = Product::with('detailOrder')
                ->where($data)
                ->offset($offset)
                ->limit($limit)
                ->get();
        } else {
            $products = Product::where($data)
                ->offset($offset)
                ->limit($limit)
                ->get();
        }

        $html = view('common.product_block', compact(  'statuses', 'products'))->render();
        return response()->json(['html'=> $html]);
    }
}
