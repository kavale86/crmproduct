<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeveloperRequest;
use App\Http\Requests\DeveloperUpdateRequest;
use App\Models\User;
use App\Services\Upload\UploadFileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    private $service;

    public function __construct(UploadFileService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = '';
        if ($request->filled('search')){
            $search = $request->input('search');
            $admins = User::where([['name', 'like', '%'.$search.'%'], ['role', '=', User::ROLE_ADMIN]])
                ->latest()
                ->paginate(15);
        } else {
            $admins = User::where('role', '=', User::ROLE_ADMIN)
                ->latest()
                ->paginate(15);
        }


        return view('pages.admin.list', compact('admins', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create');
    }

    /**
     * @param DeveloperRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(DeveloperRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'role' => User::ROLE_ADMIN,
            'description' => $request->input('description'),
            'password' => Hash::make($request->input('password')),
        ];
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatar($request);
            $data['avatar'] = $avatar;
        }
        $admin = User::create($data);

        return back()
            ->with('success',['text'=>'Admin Successfully Added!', 'id'=>$admin->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = User::find($id);
        return view('pages.admin.edit', compact('admin'));
    }

    /**
     * @param DeveloperUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DeveloperUpdateRequest $request, $id)
    {
        $admin = User::find($id);
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'description' => $request->input('description'),
        ];

        if($request->filled('password') ){
            $data['password'] = Hash::make($request->input('password'));
        }
        if($request->filled('del_avatar')){
            $this->service->deleteFile($admin->avatar);
            $admin->avatar = null;
        }
        if($request->filled('avatar')){
            $avatar = $this->service->uploadAvatar($request);
            $data['avatar'] = $avatar;
        }

        $admin->update($data);

        return back()
            ->with('success',['text'=>'Admin Successfully Added!', 'id'=>$admin->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = User::find($id);
        $this->service->deleteFile($admin->avatar);
        $admin->delete();
        return redirect('developers')->with('success','Admin deleted successfully');
    }
}
