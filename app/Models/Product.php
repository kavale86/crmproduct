<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    const WEEK_COUNT = 'week_count';
    const WEEK_AVERAGE = 'week_average';
    const WEEK_TIME = 'week_time';
    const WEEK_CRITICAL = 'week_critical';
    const WEEK_OVERTIME = 'week_overtime';

    const TITLES = [
        self::WEEK_COUNT => 'Total Products',
        self::WEEK_AVERAGE => 'Average Time',
        self::WEEK_TIME => 'Products % in Time',
        self::WEEK_CRITICAL => 'Products % Critical',
        self::WEEK_OVERTIME => 'Products % Overtime'
    ];

    protected $fillable = [ 'name', 'description', 'developer_id', 'client_id', 'status', 'status_deadline', 'status_report', 'files', 'start_date' ];

    protected $casts = [
        'status_deadline' => 'array',
        'status_report' => 'array',
        'files' => 'array',
        'start_date' => 'date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client(){
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function developer(){
        return $this->hasOne(User::class, 'id', 'developer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productStatus(){
        return $this->hasOne(Status::class, 'name', 'status');
    }

    public function getDateStatus($seconds){
        $carbon = new Carbon($seconds);

        return $carbon->format('d.m.Y');
    }


}
