<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['name', 'description', 'icon', 'index'];

    public const CONFIRM = 'confirm';
    public const LOGO = 'logo';
    public const PACKAGING = 'packaging';
    public const FINAL = 'final';
    public const LISTING = 'listing';
    public const PHOTO = 'Photo';
    public const GOODS = 'goods';

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            self::CONFIRM => ['description' => __('Project Confirmed'), 'icon' => 'fa-file-text-o'],
            self::LOGO => ['description' => __('Logo requested'), 'icon' => 'fa-plane'],
            self::PACKAGING => ['description' => __('Packaging requested'), 'icon' => 'fa-folder-open'],
            self::FINAL => ['description' => __('Final check'), 'icon' => 'fa-list'],
            self::LISTING => ['description' => __('Listing requested'), 'icon' => 'fa-file-o'],
            self::PHOTO => ['description' => __('Pictures/Photo requested'), 'icon' => 'fa-picture-o'],
            self::GOODS => ['description' => __('Goods payed'), 'icon' => 'fa-money']
        ];
    }
}
