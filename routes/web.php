<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');

    Route::resource('developers', 'DeveloperController');
    Route::resource('clients', 'ClientController')->middleware('checkAdmin');
    Route::resource('products', 'ProductController');
    Route::resource('admins', 'AdminController')->middleware('checkSuperAdmin');
    Route::resource('users', 'UserController')->middleware('checkSuperAdmin');
    Route::any('kpi', 'KpiController@index')->name('kpi')->middleware('checkAdmin');

    //home
    Route::post('filter_status', 'HomeController@filterStatus')->name('filter.status');
    Route::post('search_home', 'HomeController@searchProduct')->name('search.home');
    Route::post('load_products', 'HomeController@loadProducts');

    //upload product file
    Route::post('uploads/product/file', 'ProductController@uploadFile');
    //delete product file
    Route::post('delete/product/file', 'ProductController@deleteFile');
    //update status
    Route::post('update/product/status/{id}', 'ProductController@updateStatus')->name('update.status');


});
