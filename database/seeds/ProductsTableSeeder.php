<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class ProductsTableSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $faker = Factory::create();
        $statuses = \App\Models\Status::all();

        $product_count = 200;

        for($i=1; $i <= $product_count; $i++){
            $random = random_int(0,6);
            $status_index = $statuses[$random]->index;
            $status_name = $statuses[$random]->name;
            $statuses_deadline_array = [];
            $statuses_report_array = [];
            $day = 0;
            $day_report = 3;
            foreach ($statuses as $status){
                $statuses_deadline_array[$status->name] = now()->addDay($day)->timestamp;
                $statuses_report_array[$status->name] = $status_index >= $status->index? now()->addDay($day_report)->timestamp: null;
                $day += $day + random_int(1, 2);
                $day_report = $day_report + random_int(1, 2);
            }
            $data = [
                'name' => $faker->name,
                'developer_id' => random_int(2,3),
                'client_id' => random_int(1,3),
                'description' => $faker->text,
                'status' => $status_name,
                'status_deadline' => $statuses_deadline_array,
                'status_report' => $statuses_report_array,
                'start_date' => $statuses_report_array[\App\Models\Status::CONFIRM],
                'created_at' => now()->addDay(random_int(-60, 60))->timestamp
            ];
            \App\Models\Product::create($data);
        }
    }
}
