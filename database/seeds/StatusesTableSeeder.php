<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        foreach (\App\Models\Status::getList() as $key => $value) {
            \App\Models\Status::create(['name' => $key, 'description' => $value['description'], 'icon' => $value['icon'], 'index' => $i]);
            $i++;
        }
    }
}
