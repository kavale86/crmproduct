<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'SuperAdmin',
            'email' => 'admin@admin.com',
            'email_verified_at' => null,
            'password' => bcrypt('secret'),
            'role' => User::ROLE_SUPER_ADMIN,
            'remember_token' => null,
        ]);

        User::create([
            'name' => 'Developer1',
            'email' => 'developer1@developer.com',
            'email_verified_at' => null,
            'password' => bcrypt('secret'),
            'role' => User::ROLE_DEVELOPER,
            'remember_token' => null,
        ]);
        User::create([
            'name' => 'Developer2',
            'email' => 'develope2r@developer.com',
            'email_verified_at' => null,
            'password' => bcrypt('secret'),
            'role' => User::ROLE_DEVELOPER,
            'remember_token' => null,
        ]);

    }
}
