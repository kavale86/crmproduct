<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Client::create([
            'name' => 'Client1',
            'email' => 'client1@client.com',
        ]);

        \App\Models\Client::create([
            'name' => 'Client2',
            'email' => 'client2@client.com',
        ]);

        \App\Models\Client::create([
            'name' => 'Client3',
            'email' => 'client3@client.com',
        ]);
    }
}
